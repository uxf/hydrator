<?php

declare(strict_types=1);

namespace UXF\HydratorTests\Project\Objects;

class Disco
{
    /**
     * @param string[] $arrayA
     * @param Metal[] $arrayB
     * @param array<Metal>|null $arrayC
     * @param list<Metal>|null $arrayD
     */
    public function __construct(
        public array $arrayA,
        public array $arrayB,
        public ?array $arrayC,
        public ?array $arrayD,
    ) {
    }
}
