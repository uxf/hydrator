<?php

declare(strict_types=1);

namespace UXF\HydratorTests\Project\Objects;

final readonly class Tango
{
    /**
     * @param string[] $y
     */
    public function __construct(
        public string $x,
        public array $y,
    ) {
    }
}
