<?php

declare(strict_types=1);

namespace UXF\HydratorTests\Project\Objects;

final readonly class Grunge
{
    public function __construct(
        public float|int|string|null $a,
        public float|int|string|null $c = null,
    ) {
    }
}
