<?php

declare(strict_types=1);

namespace UXF\HydratorTests\Project\Objects\Xml;

use UXF\Hydrator\Attribute\HydratorProperty;

final readonly class DogsDto
{
    /**
     * @param DogDto[] $dogs
     */
    public function __construct(
        #[HydratorProperty('dog')]
        public array $dogs,
    ) {
    }
}
