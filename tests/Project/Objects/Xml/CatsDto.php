<?php

declare(strict_types=1);

namespace UXF\HydratorTests\Project\Objects\Xml;

use UXF\Hydrator\Attribute\HydratorProperty;

final readonly class CatsDto
{
    public function __construct(
        #[HydratorProperty('cat')]
        public CatDto $cat,
    ) {
    }
}
