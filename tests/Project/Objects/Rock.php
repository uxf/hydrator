<?php

declare(strict_types=1);

namespace UXF\HydratorTests\Project\Objects;

use DateTimeImmutable;

class Rock
{
    /**
     * @param Metal[] $metalArray
     * @param SwingEnum[] $enumArray
     */
    public function __construct(
        public Metal $metal,
        public array $metalArray,
        public SwingEnum $enum,
        public array $enumArray,
        public DateTimeImmutable $dateTime,
        public DateTimeImmutable $dateTime2,
        public ?DateTimeImmutable $dateTime3,
        public ?DateTimeImmutable $dateTime4 = null,
    ) {
    }
}
