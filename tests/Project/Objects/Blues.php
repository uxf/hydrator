<?php

declare(strict_types=1);

namespace UXF\HydratorTests\Project\Objects;

use UXF\Hydrator\Attribute\HydratorProperty;

class Blues
{
    public function __construct(
        #[HydratorProperty('@id')]
        public int $id,
    ) {
    }
}
