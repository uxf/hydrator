<?php

declare(strict_types=1);

namespace UXF\HydratorTests\Project\Objects\Family;

class CrossCountrySkiing extends Sport
{
    public function __construct(
        public readonly string $ski,
    ) {
    }
}
