<?php

declare(strict_types=1);

namespace UXF\HydratorTests\Project\Objects\Family;

class Paragliding extends Sport
{
    public function __construct(
        public readonly string $type,
        public readonly string $glider,
    ) {
    }
}
