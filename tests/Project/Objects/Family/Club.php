<?php

declare(strict_types=1);

namespace UXF\HydratorTests\Project\Objects\Family;

final readonly class Club
{
    /**
     * @param Activity[] $activities
     * @param Sport[] $sports
     */
    public function __construct(
        public array $activities,
        public array $sports,
        public Activity $activity,
        public Sport $sport,
    ) {
    }
}
