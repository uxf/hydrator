<?php

declare(strict_types=1);

namespace UXF\HydratorTests\Unit;

use phpDocumentor\Reflection\Types\Context;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use ReflectionMethod;
use UXF\Hydrator\Inspector\ArrayItemTypeDefinition;
use UXF\Hydrator\Inspector\SimpleArrayPhpDocParser;
use UXF\HydratorTests\Project\Objects\Disco;
use UXF\HydratorTests\Project\Objects\Metal;

class ArrayPhpDocParserTest extends TestCase
{
    public function testFull(): void
    {
        $parser = new SimpleArrayPhpDocParser();

        $reflectionMethod = new ReflectionMethod(Disco::class, '__construct');
        $reflectionParameters = $reflectionMethod->getParameters();

        self::assertSame(['string'], $parser->resolveTypes($reflectionParameters[0])?->types);
        self::assertSame([Metal::class], $parser->resolveTypes($reflectionParameters[1])?->types);
        self::assertSame([Metal::class], $parser->resolveTypes($reflectionParameters[2])?->types);
        self::assertSame([Metal::class], $parser->resolveTypes($reflectionParameters[3])?->types);
    }

    public function testLines(): void
    {
        $phpDoc = '/**
     * @param array<int> $aaa note
     * @param array<string> $a note
     * @param array<float> $aa note
     * @param list<int> $b1 note
     * @param list<string> $b2 note
     * @param list<float> $b3 note
     */';

        $expected = [
            'aaa' => new ArrayItemTypeDefinition(['int'], false),
            'a' => new ArrayItemTypeDefinition(['string'], false),
            'aa' => new ArrayItemTypeDefinition(['float'], false),
            'b1' => new ArrayItemTypeDefinition(['int'], false),
            'b2' => new ArrayItemTypeDefinition(['string'], false),
            'b3' => new ArrayItemTypeDefinition(['float'], false),
        ];

        $parser = new SimpleArrayPhpDocParser();
        $actual = $parser->parsePhpDoc($phpDoc, new Context(''));
        self::assertEquals($expected, $actual);
    }

    /**
     * @param array{string[], bool} $expected
     */
    #[DataProvider('getData')]
    public function testLine(string $phpDoc, array $expected): void
    {
        $parser = new SimpleArrayPhpDocParser();
        $actual = $parser->parsePhpDoc($phpDoc, new Context(''))['x'];
        self::assertSame($expected[0], $actual->types);
        self::assertSame($expected[1], $actual->nullable);
    }

    /**
     * @return array<mixed>
     */
    public static function getData(): array
    {
        return [
            ['@param string[] $x', [['string'], false]],
            ['@param string[]|null $x', [['string'], false]],
            ['@param array<string> $x', [['string'], false]],
            ['@param list<string> $x', [['string'], false]],
            ['@param array<string, int> $x', [['int'], false]],
            ['@param array<int, string> $x', [['string'], false]],
            ['@param Metal[] $x', [['Metal'], false]],
            ['@param string[]|null $x', [['string'], false]],
            ['@param Metal[]|null $x', [['Metal'], false]],
            ['@param string[]|NotSet $x', [['string'], false]],
            ['@param array<string>|NotSet $x', [['string'], false]],
            ['@param list<string>|NotSet $x', [['string'], false]],
            ['@param array<int, string>|NotSet $x', [['string'], false]],
            ['@param array<array<string, string>> $x', [['mixed'], false]],
        ];
    }
}
