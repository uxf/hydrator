<?php

declare(strict_types=1);

namespace UXF\HydratorTests\Functional;

use DateTimeImmutable;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use UXF\Hydrator\Exception\HydratorCollectionException;
use UXF\Hydrator\Exception\HydratorException;
use UXF\Hydrator\ObjectHydrator;
use UXF\Hydrator\Options;
use UXF\HydratorTests\Project\FunnyService;
use UXF\HydratorTests\Project\Objects\Blues;
use UXF\HydratorTests\Project\Objects\Family\Activity;
use UXF\HydratorTests\Project\Objects\Family\Club;
use UXF\HydratorTests\Project\Objects\Family\CrossCountrySkiing;
use UXF\HydratorTests\Project\Objects\Family\Orienteering;
use UXF\HydratorTests\Project\Objects\Family\Paragliding;
use UXF\HydratorTests\Project\Objects\Family\Sport;
use UXF\HydratorTests\Project\Objects\Grunge;
use UXF\HydratorTests\Project\Objects\IgnoredType;
use UXF\HydratorTests\Project\Objects\JazzEnum;
use UXF\HydratorTests\Project\Objects\Metal;
use UXF\HydratorTests\Project\Objects\Punk;
use UXF\HydratorTests\Project\Objects\Reggae;
use UXF\HydratorTests\Project\Objects\Rock;
use UXF\HydratorTests\Project\Objects\Ska;
use UXF\HydratorTests\Project\Objects\SwingEnum;
use UXF\HydratorTests\Project\Objects\Tango;
use UXF\HydratorTests\Project\Objects\Xml\CatDto;
use UXF\HydratorTests\Project\Objects\Xml\CatsDto;
use UXF\HydratorTests\Project\Objects\Xml\DogDto;
use UXF\HydratorTests\Project\Objects\Xml\ZooDto;
use function Safe\file_get_contents;
use function Safe\json_decode;
use function Safe\json_encode;

class SmokeTest extends KernelTestCase
{
    public function test(): void
    {
        $hydrator = $this->getHydrator();

        $punk = new Punk(
            stringX: 'A',
            intX: 1,
            floatX: 2.2,
            boolX: true,
            objectX: new Metal(1),
            arrayX: ['B'],
            arrayObjectX: [new Metal(2)],
            arrayEnum: [
                'ok' => JazzEnum::ALPHA,
            ],
            stringEnumX: JazzEnum::BRAVO,
            intEnumX: SwingEnum::ONE,
            unionX: 666,
            mixedX: ['whatever...'],
            arrayMixed: [],
            stringNull: null,
            intNull: null,
            floatNull: null,
            boolNull: null,
            objectNull: null,
            arrayNull: null,
            arrayObjectNull: null,
            arrayEnumNull: null,
            stringEnumNull: null,
            intEnumNull: null,
            unionNull: null,
            arrayMixedNull: null,
        );
        $punk->ignoredType = null;

        $data = json_decode(json_encode($punk), true);
        $result = $hydrator->hydrateArray($data, Punk::class);

        self::assertEquals($punk, $result);
    }

    public function testErrors(): void
    {
        $hydrator = $this->getHydrator();

        $errors = [];
        try {
            $hydrator->hydrateArray([
                'objectX' => [],
                'arrayObjectX' => [[]],
                'arrayEnum' => [
                    'wow' => 5,
                ],
                'stringNull' => 1,
                'intNull' => 'x',
                'floatNull' => 'y',
                'boolNull' => [],
                'objectNull' => 1,
                'arrayNull' => 'hello',
                'arrayObjectNull' => 1,
                'arrayEnumNull' => ['C', 1],
                'stringEnumNull' => 'X',
                'intEnumNull' => 2,
                'unionNull' => true,
                'arrayMixedNull' => 1,
            ], Punk::class);
        } catch (HydratorException $e) {
            $errors = $e->errors;
        }

        self::assertSame([
            'stringX' => ["Missing value"],
            'intX' => ["Missing value"],
            'floatX' => ["Missing value"],
            'boolX' => ["Missing value"],
            'objectX.id' => ["Missing value"],
            'arrayX' => ["Missing value"],
            'arrayObjectX[0].id' => ["Missing value"],
            'arrayEnum.wow' => ["Invalid value [int given]"],
            'stringEnumX' => ["Missing value"],
            'intEnumX' => ["Missing value"],
            'unionX' => ["Missing value"],
            'mixedX' => ["Missing value"],
            'arrayMixed' => ["Missing value"],
            'stringNull' => ["Invalid value (string required) [int given]"],
            'intNull' => ["Invalid value (int required) [string given]"],
            'floatNull' => ["Invalid value (float required) [string given]"],
            'boolNull' => ["Invalid value (bool required) [array given]"],
            'objectNull' => ["Invalid value [int given]"],
            'arrayNull' => ["Invalid value (array required) [string given]"],
            'arrayObjectNull' => ["Invalid value (array required) [int given]"],
            'arrayEnumNull[0]' => ["Value is not supported [Supported values are A, B => 'C' value given]"],
            'arrayEnumNull[1]' => ["Invalid value [int given]"],
            'stringEnumNull' => ["Value is not supported [Supported values are A, B => 'X' value given]"],
            'intEnumNull' => ["Value is not supported [Supported values are 0, 1 => '2' value given]"],
            'unionNull' => ["Invalid value [Supported types are [string, int] => bool given]"],
            'arrayMixedNull' => ["Invalid value (array required) [int given]"],
        ], $errors);
    }

    public function testArrayErrors(): void
    {
        $hydrator = $this->getHydrator();

        $exceptions = [];
        $msg = '';
        try {
            $hydrator->hydrateArrays([
                [
                    'id' => null,
                ],
                [
                    'id' => 'ok',
                ],
                [],
            ], Metal::class);
        } catch (HydratorCollectionException $e) {
            $exceptions = $e->exceptions;
            $msg = $e->getMessage();
        }

        self::assertSame([
            '{"id":["Invalid value (int required) [null given]"]}',
            '{"id":["Invalid value (int required) [string given]"]}',
            '{"id":["Missing value"]}',
        ], json_decode($msg));

        self::assertCount(3, $exceptions);
        self::assertSame([
            'id' => [
                'Invalid value (int required) [null given]',
            ],
        ], $exceptions[0]->errors);
        self::assertSame([
            'id' => [
                'Invalid value (int required) [string given]',
            ],
        ], $exceptions[1]->errors);
        self::assertSame([
            'id' => [
                'Missing value',
            ],
        ], $exceptions[2]->errors);
    }

    public function testAlreadyCaster(): void
    {
        $hydrator = $this->getHydrator();

        $data = [
            'metal' => new Metal(1),
            'metalArray' => [new Metal(2)],
            'enum' => SwingEnum::ONE,
            'enumArray' => [SwingEnum::ZERO],
            'dateTime' => new DateTimeImmutable('2022-01-28T20:06:21+00:00'),
            'dateTime2' => '2022-01-28T20:06:21+00:00',
            'dateTime3' => null,
        ];

        $result = $hydrator->hydrateArray($data, Rock::class);

        $rock = new Rock(
            metal: new Metal(1),
            metalArray: [new Metal(2)],
            enum: SwingEnum::ONE,
            enumArray: [SwingEnum::ZERO],
            dateTime: new DateTimeImmutable('2022-01-28T20:06:21+00:00'),
            dateTime2: new DateTimeImmutable('2022-01-28T20:06:21+00:00'),
            dateTime3: null,
        );

        self::assertEquals($rock, $result);
    }

    public function testUnionCaster(): void
    {
        $hydrator = $this->getHydrator();

        // interface
        $data = [
            'type' => 'p',
            'glider' => 'Axis',
        ];
        $result = $hydrator->hydrateArray($data, Activity::class);
        self::assertInstanceOf(Paragliding::class, $result);
        self::assertSame('p', $result->type);
        self::assertSame('Axis', $result->glider);

        $data = [
            'type' => 'o',
            'card' => 123456,
        ];
        $result = $hydrator->hydrateArray($data, Activity::class);
        self::assertInstanceOf(Orienteering::class, $result);
        self::assertSame(123456, $result->card);

        // abstract class
        $data = [
            'type' => 'c',
            'ski' => 'Atomic',
        ];
        $result = $hydrator->hydrateArray($data, Sport::class);
        self::assertInstanceOf(CrossCountrySkiing::class, $result);
        self::assertSame('Atomic', $result->ski);

        $data = [
            'type' => 'p',
            'glider' => 'Axis',
        ];
        $result = $hydrator->hydrateArray($data, Sport::class);
        self::assertInstanceOf(Paragliding::class, $result);
        self::assertSame('p', $result->type);
        self::assertSame('Axis', $result->glider);

        $data = [
            'type' => 'o',
            'card' => 123456,
        ];
        $result = $hydrator->hydrateArray($data, Sport::class);
        self::assertInstanceOf(Orienteering::class, $result);
        self::assertSame(123456, $result->card);

        $data = [
            'activities' => [
                [
                    'type' => 'p',
                    'glider' => 'Gin',
                ],
                [
                    'type' => 'o',
                    'card' => 777,
                ],
            ],
            'sports' => [
                [
                    'type' => 'c',
                    'ski' => 'Fisher',
                ],
            ],
            'activity' => [
                'type' => 'o',
                'card' => 123456,
            ],
            'sport' => [
                'type' => 'o',
                'card' => 666,
            ],
        ];
        $result = $hydrator->hydrateArray($data, Club::class);
        self::assertInstanceOf(Paragliding::class, $result->activities[0]);
        self::assertInstanceOf(Orienteering::class, $result->activities[1]);
        self::assertInstanceOf(CrossCountrySkiing::class, $result->sports[0]);
    }

    public function testUnionErrorMissingTypeProperty(): void
    {
        $hydrator = $this->getHydrator();

        try {
            $data = [
                'card' => 123456,
            ];
            $hydrator->hydrateArray($data, Sport::class);
        } catch (HydratorException $e) {
            self::assertSame([
                'type' => ['Missing value'],
            ], $e->errors);
        }
    }

    public function testUnionErrorInvalidTypeProperty(): void
    {
        $hydrator = $this->getHydrator();

        try {
            $data = [
                'type' => '?',
            ];
            $hydrator->hydrateArray($data, Sport::class);
        } catch (HydratorException $e) {
            self::assertSame([
                'type' => ["Invalid value [Supported values are c, o, p => '?' value given]"],
            ], $e->errors);
        }
    }

    public function testUnionErrorPath(): void
    {
        $hydrator = $this->getHydrator();

        try {
            $data = [
                'activities' => [[]],
                'sports' => [[
                    'type' => 'X',
                ]],
                'activity' => [],
                'sport' => [
                    'type' => 'Y',
                ],
            ];
            $hydrator->hydrateArray($data, Club::class);
        } catch (HydratorException $e) {
            self::assertSame([
                'activities[0].type' => [
                    'Missing value',
                ],
                'sports[0].type' => [
                    "Invalid value [Supported values are c, o, p => 'X' value given]",
                ],
                'activity.type' => [
                    'Missing value',
                ],
                'sport.type' => [
                    "Invalid value [Supported values are c, o, p => 'Y' value given]",
                ],
            ], $e->errors);
        }
    }

    public function testFloat(): void
    {
        $hydrator = $this->getHydrator();

        $data = [
            'float' => '1.2345',
        ];
        $result = $hydrator->hydrateArray($data, Reggae::class);
        self::assertSame(1.2345, $result->float);
    }

    public function testUnionScalar(): void
    {
        $hydrator = $this->getHydrator();
        $options = new Options('S', [
            'allow_fallback' => false,
        ]);

        $result = $hydrator->hydrateArray([
            'a' => 1.2345,
        ], Grunge::class, $options);
        self::assertSame(1.2345, $result->a);

        $result = $hydrator->hydrateArray([
            'a' => 'hello',
        ], Grunge::class, $options);
        self::assertSame('hello', $result->a);

        $result = $hydrator->hydrateArray([
            'a' => -100,
        ], Grunge::class, $options);
        self::assertSame(-100, $result->a);

        $result = $hydrator->hydrateArray([
            'a' => null,
        ], Grunge::class, $options);
        self::assertNull($result->a);
    }

    public function testLaxString(): void
    {
        $hydrator = $this->getHydrator();
        $options = new Options('S', [
            'allow_lax_string' => true,
        ]);

        // integer
        $data = [
            'string' => 1,
            'null' => 2,
        ];
        $result = $hydrator->hydrateArray($data, Ska::class, $options);
        self::assertSame('1', $result->string);
        self::assertSame('2', $result->null);

        // Stringable
        $data = [
            'string' => new Ska('wow', null),
            'null' => new Ska('wow2', null),
        ];
        $result = $hydrator->hydrateArray($data, Ska::class, $options);
        self::assertSame('wow', $result->string);
        self::assertSame('wow2', $result->null);

        // error with null
        $data = [
            'string' => null,
            'null' => 666,
        ];

        $errors = null;
        try {
            $hydrator->hydrateArray($data, Ska::class, $options);
        } catch (HydratorException $e) {
            $errors = $e->errors;
        }
        self::assertSame([
            'string' => ['Invalid value (string required) [null given]'],
        ], $errors);
    }

    public function testTrimString(): void
    {
        $hydrator = $this->getHydrator();
        $options = new Options('S', [
            'allow_trim_string' => true,
        ]);

        $data = [
            'x' => ' A ',
            'y' => [' B '],
        ];
        $result = $hydrator->hydrateArray($data, Tango::class, $options);
        self::assertSame('A', $result->x);
        self::assertSame(['B'], $result->y);
    }

    public function testNullableOptional(): void
    {
        $hydrator = $this->getHydrator();
        $options = new Options('N', [
            'nullable_optional' => true,
        ]);

        $data = [
            'stringX' => '',
            'intX' => 1,
            'floatX' => 1,
            'boolX' => true,
            'objectX' => [
                'id' => 1,
            ],
            'arrayX' => [],
            'arrayObjectX' => [],
            'arrayEnum' => [],
            'stringEnumX' => 'A',
            'intEnumX' => 1,
            'unionX' => '',
            'mixedX' => '',
            'arrayMixed' => [],
        ];
        $result = $hydrator->hydrateArray($data, Punk::class, $options);
        self::assertInstanceOf(IgnoredType::class, $result->ignoredType);
    }

    public function testPropertyInfo(): void
    {
        $hydrator = $this->getHydrator();

        $data = [
            '@id' => 1,
        ];
        $result = $hydrator->hydrateArray($data, Blues::class);
        self::assertSame(1, $result->id);
    }

    public function testXml(): void
    {
        $hydrator = $this->getHydrator();

        $data = (new XmlEncoder())->decode(file_get_contents(__DIR__ . '/zoo.xml'), 'xml');

        // test encoder BC
        self::assertSame([
            'dog' => [
                'id' => '1',
                'name' => 'Alpha',
                'price' => '100.1',
                'valid' => 'true',
                'item' => 'a',
                'cat' => [
                    '@ID' => 1,
                    '#' => 'CAT',
                ],
            ],
            'emptyDog' => '',
            'oneDogs' => [
                'dog' => [
                    'id' => '2',
                    'name' => 'Bravo',
                    'price' => '100.2',
                    'valid' => '',
                    'item' => ['a', 'b'],
                    'cat' => [
                        [
                            '@ID' => 2,
                            '#' => 'CAT 2',
                        ],
                        'CAT 3',
                    ],
                ],
            ],
            'twoDogs' => [
                'dog' => [
                    [
                        'id' => '3',
                        'name' => 'Charlie',
                        'price' => '100.3',
                        'item' => '',
                    ],
                    [
                        'id' => '4',
                        'name' => 'Delta',
                        'price' => '100.4',
                    ],
                ],
            ],
            'emptyDogs' => '',
            'naughtyDogs' => [
                'dog' => '',
            ],
        ], $data);

        // run
        $result = $hydrator->hydrateArray($data, ZooDto::class, new Options('xml', [
            'xml_mode' => true,
        ]));

        $dog = new DogDto(1, 'Alpha', 100.1, true, ['a'], [new CatDto('CAT', 1)]);
        self::assertEquals($dog, $result->dog);
        self::assertNull($result->emptyDog);
        self::assertCount(1, $result->oneDogs->dogs);
        self::assertCount(2, $result->oneDogs->dogs[0]->cats);
        self::assertCount(2, $result->twoDogs->dogs);
        self::assertSame([], $result->twoDogs->dogs[0]->items);
        self::assertNull($result->emptyDogs);
        self::assertSame([], $result->naughtyDogs->dogs);
    }

    public function testXmlContent(): void
    {
        $hydrator = $this->getHydrator();

        $data = [
            'cat' => [
                '@ID' => 1,
                '#' => 'CAT',
            ],
        ];
        $result = $hydrator->hydrateArray($data, CatsDto::class, new Options('xml', [
            'xml_mode' => true,
        ]));

        self::assertSame(1, $result->cat->id);
        self::assertSame('CAT', $result->cat->text);

        $data = [
            'cat' => 'CAT',
        ];
        $result = $hydrator->hydrateArray($data, CatsDto::class, new Options('xml', [
            'xml_mode' => true,
        ]));

        self::assertNull($result->cat->id);
        self::assertSame('CAT', $result->cat->text);
    }

    private function getHydrator(): ObjectHydrator
    {
        $container = self::getContainer();
        $funny = $container->get(FunnyService::class);
        assert($funny instanceof FunnyService);
        return $funny->getObjectHydrator();
    }
}
