<?php

declare(strict_types=1);

namespace UXF\Hydrator\Translator;

final readonly class DefaultTranslator implements Translator
{
    private const array Translations = [
        'core.invalid_value' => 'Invalid value',
        'core.missing_value' => 'Missing value',
        'core.array_invalid_value' => 'Invalid value (array required)',
        'date_time_immutable.invalid_format' => 'Invalid format',
        'enum.invalid_value' => 'Value is not supported',
        'int.invalid_value' => 'Invalid value (int required)',
        'float.invalid_value' => 'Invalid value (float required)',
        'bool.invalid_value' => 'Invalid value (bool required)',
        'string.invalid_value' => 'Invalid value (string required)',
    ];

    public function trans(string $key, ?ErrorInfo $info = null): string
    {
        if (array_key_exists($key, self::Translations)) {
            return self::Translations[$key] . self::resolveMessage($info);
        }

        return self::Translations[$key] ?? $key;
    }

    /**
     * @internal
     */
    public static function resolveMessage(?ErrorInfo $info): string
    {
        if ($info === null) {
            return '';
        }

        if ($info->supportedTypes !== null) {
            return ' [Supported types are [' . implode(', ', $info->supportedTypes) . '] => ' . get_debug_type($info->currentValue) . ' given]';
        }

        if ($info->supportedValues !== null) {
            return ' [Supported values are ' . implode(', ', $info->supportedValues) . ' => \'' . $info->currentValue . '\' value given]';
        }

        if ($info->supportedFormat !== null) {
            return ' [Supported format is ' . $info->supportedFormat . ' => \'' . $info->currentValue . '\' value given]';
        }

        return ' [' . get_debug_type($info->currentValue) . ' given]';
    }
}
