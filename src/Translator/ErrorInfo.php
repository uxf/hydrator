<?php

declare(strict_types=1);

namespace UXF\Hydrator\Translator;

final readonly class ErrorInfo
{
    /**
     * @param array<mixed>|null $supportedTypes
     * @param array<mixed>|null $supportedValues
     */
    public function __construct(
        public mixed $currentValue,
        public ?string $supportedFormat = null,
        public ?array $supportedTypes = null,
        public ?array $supportedValues = null,
    ) {
    }
}
