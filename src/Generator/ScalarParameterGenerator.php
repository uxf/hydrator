<?php

declare(strict_types=1);

namespace UXF\Hydrator\Generator;

use Nette\PhpGenerator\Dumper;
use Nette\Utils\Strings;
use UXF\Hydrator\Inspector\ParameterDefinition;
use UXF\Hydrator\Options;
use UXF\Hydrator\ParameterGenerator;

final readonly class ScalarParameterGenerator implements ParameterGenerator
{
    public function generate(ParameterDefinition $definition, Options $options): string
    {
        $name = $definition->name;
        $trim = (bool) ($options['allow_trim_string'] ?? false);
        $xml = (bool) ($options['xml_mode'] ?? false);

        $body = '// ' . __CLASS__ . "\n";
        $body .= "if (false) {\n";

        foreach ($definition->types as $type) {
            if ($type === 'int') {
                if ($xml) {
                    $body .= "} elseif (is_numeric(\$data[\$name])) {\n";
                    $body .= "    \$_$name = (int) \$data[\$name];\n";
                } else {
                    $body .= "} elseif (is_int(\$data[\$name])) {\n";
                    $body .= "    \$_$name = \$data[\$name];\n";
                }
            } elseif ($type === 'float') {
                $body .= "} elseif (is_numeric(\$data[\$name])) {\n";
                $body .= "    \$_$name = (float) \$data[\$name];\n";
            } elseif ($type === 'bool') {
                if ($xml) {
                    $body .= "} elseif (true) {\n";
                    $body .= "    \$_$name = in_array(\$data[\$name], ['true', '', '1'], true);\n";
                } else {
                    $body .= "} elseif (is_bool(\$data[\$name])) {\n";
                    $body .= "    \$_$name = \$data[\$name];\n";
                }
            } elseif ($type === 'string') {
                $body .= "} elseif (is_string(\$data[\$name])) {\n";
                $body .= $trim
                    ? "    \$_$name = \\" . Strings::class . "::trim(\$data[\$name]);\n"
                    : "    \$_$name = \$data[\$name];\n";

                // allow lax string
                if (($options['allow_lax_string'] ?? false) === true) {
                    $body .= "} elseif (\$data[\$name] !== null && (is_scalar(\$data[\$name]) || \$data[\$name] instanceof \Stringable)) {\n";
                    $body .= $trim
                        ? "    \$_$name = \\" . Strings::class . "::trim((string) \$data[\$name]);\n"
                        : "    \$_$name = (string) \$data[\$name];\n";
                }
            }
        }

        if ($definition->nullable) {
            $body .= "} elseif (\$data[\$name] === null) {\n";
            $body .= "    \$_$name = null;\n";
        }

        $body .= "} else {\n";
        if ($definition->isUnion()) {
            $supportedTypes = (new Dumper())->dump($definition->types);
            $body .= "    \$errors[\$path . \$name][] = \$this->translator->trans('core.invalid_value', new ErrorInfo(\$data[\$name], supportedTypes: $supportedTypes));\n";
        } else {
            $body .= "    \$errors[\$path . \$name][] = \$this->translator->trans('{$definition->getFirstType()}.invalid_value', new ErrorInfo(\$data[\$name]));\n";
        }
        $body .= "}";

        return $body;
    }

    public function supports(ParameterDefinition $definition, Options $options): bool
    {
        if ($definition->array) {
            return false;
        }

        foreach ($definition->types as $type) {
            if (!in_array($type, ['int', 'float', 'bool', 'string'], true)) {
                return false;
            }
        }

        return true;
    }

    public static function getDefaultPriority(): int
    {
        return 100;
    }
}
