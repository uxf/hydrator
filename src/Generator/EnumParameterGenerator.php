<?php

declare(strict_types=1);

namespace UXF\Hydrator\Generator;

use BackedEnum;
use Nette\PhpGenerator\Dumper;
use UXF\Hydrator\Inspector\ParameterDefinition;
use UXF\Hydrator\Options;
use UXF\Hydrator\ParameterGenerator;

final readonly class EnumParameterGenerator implements ParameterGenerator
{
    public function generate(ParameterDefinition $definition, Options $options): string
    {
        $typeName = $definition->getFirstType();
        $phpType = is_string($typeName::cases()[0]->value) ? 'string' : 'int';
        $supportedValues = (new Dumper())->dump(array_map(static fn (BackedEnum $case) => $case->value, $typeName::cases()));

        $name = $definition->name;

        $body = '// ' . __CLASS__ . " - $phpType\n";
        $body .= "if (\$data[\$name] instanceof \\$typeName) {\n";
        $body .= "    \$_$name = \$data[\$name];\n";
        $body .= "} elseif (is_$phpType(\$data[\$name])) {\n";
        $body .= "    \$_$name = \\$typeName::tryFrom(\$data[\$name]);\n";
        $body .= "    if (\$_$name === null) {\n";
        $body .= "        \$errors[\$path . \$name][] = \$this->translator->trans('enum.invalid_value', new ErrorInfo(\$data[\$name], supportedValues: $supportedValues));\n";
        $body .= "    }\n";

        if ($definition->nullable) {
            $body .= "} elseif (\$data[\$name] === null) {\n";
            $body .= "    \$_$name = null;\n";
        }

        $body .= "} else {\n";
        $body .= "    \$errors[\$path . \$name][] = \$this->translator->trans('core.invalid_value', new ErrorInfo(\$data[\$name]));\n";
        $body .= "}\n";
        return $body;
    }

    public function supports(ParameterDefinition $definition, Options $options): bool
    {
        return !$definition->isUnion() && !$definition->array && enum_exists($definition->getFirstType());
    }

    public static function getDefaultPriority(): int
    {
        return 200;
    }
}
