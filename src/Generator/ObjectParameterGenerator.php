<?php

declare(strict_types=1);

namespace UXF\Hydrator\Generator;

use ReflectionClass;
use UXF\Hydrator\Attribute\HydratorXml;
use UXF\Hydrator\Inspector\ParameterDefinition;
use UXF\Hydrator\Options;
use UXF\Hydrator\ParameterGenerator;

final readonly class ObjectParameterGenerator implements ParameterGenerator
{
    public function generate(ParameterDefinition $definition, Options $options): string
    {
        $name = $definition->name;
        $typeName = $definition->getFirstType();
        $xml = (bool) ($options['xml_mode'] ?? false);

        $body = '// ' . __CLASS__ . "\n";
        $body .= "if (\$data[\$name] instanceof \\$typeName) {\n";
        $body .= "    \$_$name = \$data[\$name];\n";
        $body .= "} elseif (is_array(\$data[\$name])) {\n";
        $body .= "    try {\n";
        $body .= "        \$_$name = \$this->typeCasterProvider->get('$typeName', \$this->options)->cast(\$data[\$name], \$path . \$name . '.');\n";
        $body .= "    } catch (HydratorException \$e) {\n";
        $body .= "        \$errors += \$e->errors;\n";
        $body .= "    }\n";

        if ($definition->nullable) {
            $body .= "} elseif (\$data[\$name] === null) {\n";
            $body .= "    \$_$name = null;\n";

            if ($xml) {
                $body .= "} elseif (\$data[\$name] === '') {\n";
                $body .= "    \$_$name = null;\n";
            }
        }

        if ($xml) {
            $type = $definition->getFirstType();
            assert(class_exists($type));
            $xmlAttr = (new ReflectionClass($type))->getAttributes(HydratorXml::class)[0] ?? null;
            if ($xmlAttr !== null) {
                $attr = $xmlAttr->newInstance();
                $body .= "} elseif (is_string(\$data[\$name])) {\n";
                $body .= "    \$_{$name} = new \\{$type}({$attr->dataProperty}: \$data[\$name]);\n";
            }
        }

        $body .= "} else {\n";
        $body .= "    \$errors[\$path . \$name][] = \$this->translator->trans('core.invalid_value', new ErrorInfo(\$data[\$name]));\n";
        $body .= "}\n";

        return $body;
    }

    public function supports(ParameterDefinition $definition, Options $options): bool
    {
        $type = $definition->getFirstType();
        return !$definition->array && !$definition->isUnion() && (class_exists($type) || interface_exists($type));
    }

    public static function getDefaultPriority(): int
    {
        return 0;
    }
}
