<?php

declare(strict_types=1);

namespace UXF\Hydrator\Generator;

use UXF\Hydrator\Inspector\ParameterDefinition;
use UXF\Hydrator\Options;
use UXF\Hydrator\ParameterGenerator;

final readonly class FallbackParameterGenerator implements ParameterGenerator
{
    public function generate(ParameterDefinition $definition, Options $options): string
    {
        $name = $definition->name;

        $body = '// ' . __CLASS__ . "\n";

        if (!$definition->array) {
            $body .= "\$_$name = \$data[\$name];";
        } else {
            $body .= "if (is_array(\$data[\$name])) {\n";
            $body .= "    \$_$name = \$data[\$name];\n";

            if ($definition->nullable) {
                $body .= "} elseif (\$data[\$name] === null) {\n";
                $body .= "    \$_$name = null;\n";
            }

            $body .= "} else {\n";
            $body .= "    \$errors[\$path . \$name][] = \$this->translator->trans('core.array_invalid_value', new ErrorInfo(\$data[\$name]));\n";
            $body .= "}\n";
        }

        return $body;
    }

    public function supports(ParameterDefinition $definition, Options $options): bool
    {
        return ($options['allow_fallback'] ?? false) === true;
    }

    public static function getDefaultPriority(): int
    {
        return -1024;
    }
}
