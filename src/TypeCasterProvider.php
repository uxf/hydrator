<?php

declare(strict_types=1);

namespace UXF\Hydrator;

use LogicException;
use Psr\Container\ContainerInterface;
use RuntimeException;
use UXF\Hydrator\Generator\Generator;
use UXF\Hydrator\Translator\Translator;
use UXF\Hydrator\Utils\ClassNameHelper;

final class TypeCasterProvider
{
    /** @var array<string, TypeCaster<object>> */
    private array $casters = [];

    public function __construct(
        private readonly bool $overwrite,
        private readonly string $typeCasterDir,
        private readonly Generator $generator,
        private readonly ContainerInterface $container,
        private readonly Translator $translator,
    ) {
    }

    /**
     * @template T of object
     * @param class-string<T> $className
     * @return TypeCaster<T>
     */
    public function get(string $className, Options $options): TypeCaster
    {
        if (!isset($this->casters[$className])) {
            $typeCasterName = ClassNameHelper::convertToTypeCasterName($className);
            $fullName = "\\UxfGenerator\\$options->name\\$typeCasterName";

            // manual loader
            if (!class_exists($fullName, false)) {
                $filePath = "$this->typeCasterDir/$options->name/$typeCasterName.php";
                if ($this->overwrite || (@include $filePath) === false) {
                    $code = $this->generator->generateFile($className, $options);

                    $lock = $this->acquireLock("$filePath.lock");
                    file_put_contents($filePath, $code);

                    if ((include $filePath) === false) {
                        throw new RuntimeException("Unable to load '$filePath'.");
                    }

                    flock($lock, LOCK_UN);
                }
            }

            $typeCaster = new $fullName($this, $this->container, $this->translator, $options);
            if (!$typeCaster instanceof TypeCaster) {
                throw new LogicException();
            }

            $this->casters[$className] = $typeCaster;
        }

        return $this->casters[$className];
    }

    /**
     * Inspired by https://github.com/nette/latte/blob/master/src/Latte/Engine.php
     * @return resource
     */
    private function acquireLock(string $file)
    {
        $dir = dirname($file);
        if (!is_dir($dir) && !@mkdir($dir, 0777, true) && !is_dir($dir)) { // @ - dir may already exist
            throw new RuntimeException("Unable to create directory '$dir'.");
        }

        $handle = @fopen($file, 'wb'); // @ is escalated to exception
        if ($handle === false) {
            throw new RuntimeException("Unable to create file '$file'.");
        }

        if (!@flock($handle, LOCK_EX)) { // @ is escalated to exception
            throw new RuntimeException("Unable to acquire exclusive lock on file '$file'.");
        }

        return $handle;
    }
}
