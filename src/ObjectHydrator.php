<?php

declare(strict_types=1);

namespace UXF\Hydrator;

use UXF\Hydrator\Exception\HydratorCollectionException;
use UXF\Hydrator\Exception\HydratorException;

final readonly class ObjectHydrator
{
    public function __construct(
        private TypeCasterProvider $typeCasterProvider,
        private Options $options = new Options('Default'),
    ) {
    }

    /**
     * @param array<string, mixed> $options
     */
    public static function create(TypeCasterProvider $typeCasterProvider, array $options, string $name = 'Default'): self
    {
        return new self($typeCasterProvider, new Options($name, $options));
    }

    /**
     * @template T of object
     * @param array<mixed> $data
     * @param class-string<T> $class
     * @return T
     */
    public function hydrateArray(array $data, string $class, ?Options $options = null): object
    {
        $options = $options === null ? $this->options : $options->merge($this->options);
        return $this->typeCasterProvider->get($class, $options)->cast($data, '');
    }

    /**
     * @template T of object
     * @param array<array<mixed>> $data
     * @param class-string<T> $class
     * @return array<T>
     */
    public function hydrateArrays(array $data, string $class, ?Options $options = null): array
    {
        $result = [];
        $exceptions = [];
        foreach ($data as $key => $item) {
            try {
                $result[$key] = $this->hydrateArray($item, $class, $options);
            } catch (HydratorException $e) {
                $exceptions[$key] = $e;
            }
        }

        if ($exceptions !== []) {
            throw new HydratorCollectionException($exceptions);
        }

        return $result;
    }
}
