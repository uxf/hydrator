<?php

declare(strict_types=1);

namespace UXF\Hydrator\Exception;

use RuntimeException;
use UXF\Hydrator\Inspector\ParameterDefinition;
use UXF\Hydrator\Options;

final class UnsupportedParameterDefinitionException extends RuntimeException
{
    public function __construct(public readonly ParameterDefinition $definition, public readonly Options $options)
    {
        parent::__construct();
    }
}
