<?php

declare(strict_types=1);

namespace UXF\Hydrator\Exception;

use Exception;
use function Safe\json_encode;

final class HydratorException extends Exception implements HydratorCoreException
{
    /**
     * @param array<string, array<string>> $errors
     */
    public function __construct(public readonly array $errors)
    {
        parent::__construct(json_encode($errors));
    }
}
