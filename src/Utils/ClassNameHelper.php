<?php

declare(strict_types=1);

namespace UXF\Hydrator\Utils;

final readonly class ClassNameHelper
{
    public static function convertToTypeCasterName(string $className): string
    {
        return trim(str_replace('\\', '_', $className), '_') . '__TypeCaster';
    }
}
