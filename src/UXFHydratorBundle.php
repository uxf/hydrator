<?php

declare(strict_types=1);

namespace UXF\Hydrator;

use Symfony\Component\Config\Definition\Configurator\DefinitionConfigurator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;
use UXF\Hydrator\Translator\DefaultTranslator;
use UXF\Hydrator\Translator\Translator;

final class UXFHydratorBundle extends AbstractBundle
{
    protected string $extensionAlias = 'uxf_hydrator';

    public function configure(DefinitionConfigurator $definition): void
    {
        $definition->rootNode()
            ->children()
                ->scalarNode('translator')->defaultValue(DefaultTranslator::class)->end()
                ->arrayNode('ignored_types')
                    ->scalarPrototype()->end()
                ->end()
                ->booleanNode('overwrite')
                    ->defaultValue('%kernel.debug%')
                ->end()
                ->arrayNode('default_options')
                    ->scalarPrototype()->end()
                ->end()
            ->end();
    }

    /**
     * @param array<mixed> $config
     */
    public function loadExtension(array $config, ContainerConfigurator $container, ContainerBuilder $builder): void
    {
        $container->import(__DIR__ . '/../config/services.php');

        $parameters = $container->parameters();
        $parameters->set('uxf_hydrator.ignored_types', $config['ignored_types'] ?? []);
        $parameters->set('uxf_hydrator.overwrite', $config['overwrite']);
        $parameters->set('uxf_hydrator.default_options', $config['default_options'] ?? []);

        $container->services()->set(Translator::class, $config['translator']);

        $builder->registerForAutoconfiguration(ParameterGenerator::class)
            ->addTag('uxf_hydrator.parameter_generator');
    }
}
