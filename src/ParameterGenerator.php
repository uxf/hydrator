<?php

declare(strict_types=1);

namespace UXF\Hydrator;

use UXF\Hydrator\Inspector\ParameterDefinition;

interface ParameterGenerator
{
    public function generate(ParameterDefinition $definition, Options $options): string;

    public function supports(ParameterDefinition $definition, Options $options): bool;

    public static function getDefaultPriority(): int;
}
