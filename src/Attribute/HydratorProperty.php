<?php

declare(strict_types=1);

namespace UXF\Hydrator\Attribute;

use Attribute;

#[Attribute(Attribute::TARGET_PARAMETER | Attribute::TARGET_PROPERTY)]
final readonly class HydratorProperty
{
    public function __construct(
        public string $name = '',
    ) {
    }
}
