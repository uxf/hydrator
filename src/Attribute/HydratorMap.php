<?php

declare(strict_types=1);

namespace UXF\Hydrator\Attribute;

use Attribute;

#[Attribute(Attribute::TARGET_CLASS)]
final readonly class HydratorMap
{
    /**
     * @param array<string, class-string> $matrix
     */
    public function __construct(
        public string $property,
        public array $matrix,
    ) {
    }
}
