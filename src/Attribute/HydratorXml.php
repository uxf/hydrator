<?php

declare(strict_types=1);

namespace UXF\Hydrator\Attribute;

use Attribute;

#[Attribute(Attribute::TARGET_CLASS)]
final readonly class HydratorXml
{
    public function __construct(
        public string $dataProperty,
    ) {
    }
}
