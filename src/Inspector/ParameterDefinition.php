<?php

declare(strict_types=1);

namespace UXF\Hydrator\Inspector;

use ReflectionAttribute;
use UXF\Hydrator\Attribute\HydratorProperty;

final readonly class ParameterDefinition
{
    /**
     * @param string[] $types
     * @param ReflectionAttribute<object>[] $attributes
     */
    public function __construct(
        public string $name,
        public array $types,
        public bool $array,
        public bool $optional,
        public bool $nullable,
        public bool $arrayItemNullable,
        public mixed $defaultValue,
        public ?HydratorProperty $propertyInfo,
        public array $attributes,
    ) {
    }

    public function isUnion(): bool
    {
        return count($this->types) !== 1;
    }

    public function getFirstType(): string
    {
        return $this->types[0];
    }
}
